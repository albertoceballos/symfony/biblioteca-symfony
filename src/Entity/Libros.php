<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Libros
 *
 * @ORM\Table(name="libros", indexes={@ORM\Index(name="id_editorial", columns={"id_editorial"}), @ORM\Index(name="id_coleccion", columns={"id_coleccion"})})
 * @ORM\Entity(repositoryClass="App\Repository\LibrosRepository")
 */
class Libros
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_libro", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLibro;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="anio", type="string", length=4, nullable=true)
     */
    private $anio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ISBN", type="string", length=255, nullable=true)
     */
    private $isbn;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="leido", type="boolean", nullable=true)
     */
    private $leido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="foto", type="blob", length=65535, nullable=true)
     */
    private $foto;

    /**
     * @var \Editoriales
     *
     * @ORM\ManyToOne(targetEntity="Editoriales", inversedBy="libros")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_editorial", referencedColumnName="id_editorial")
     * })
     */
    private $idEditorial;

    /**
     * @var \Colecciones
     *
     * @ORM\ManyToOne(targetEntity="Colecciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coleccion", referencedColumnName="id_coleccion")
     * })
     */
    private $idColeccion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Autores", mappedBy="idLibro")
     */
    private $idAutor;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Tematicas", inversedBy="idLibro")
     * @ORM\JoinTable(name="reproducen",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_libro", referencedColumnName="id_libro")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_tematica", referencedColumnName="id_tematica")
     *   }
     * )
     */
    private $idTematica;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idAutor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idTematica = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdLibro()
    {
        return $this->idLibro;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getAnio()
    {
        return $this->anio;
    }

    public function setAnio(string $anio)
    {
        $this->anio = $anio;

        return $this;
    }

    public function getIsbn()
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getLeido()
    {
        return $this->leido;
    }

    public function setLeido(bool $leido): self
    {
        $this->leido = $leido;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFoto()
    {
        return $this->foto;
    }

    public function setFoto($foto): self
    {
        $this->foto = $foto;

        return $this;
    }

    public function getIdEditorial()
    {
        return $this->idEditorial;
    }

    public function setIdEditorial(Editoriales $idEditorial): self
    {
        $this->idEditorial = $idEditorial;

        return $this;
    }

    public function getIdColeccion()
    {
        return $this->idColeccion;
    }

    public function setIdColeccion(Colecciones $idColeccion): self
    {
        $this->idColeccion = $idColeccion;

        return $this;
    }

    /**
     * @return Collection|Autores[]
     */
    public function getIdAutor(): Collection
    {
        return $this->idAutor;
    }

    public function addIdAutor(Autores $idAutor): self
    {
        if (!$this->idAutor->contains($idAutor)) {
            $this->idAutor[] = $idAutor;
            $idAutor->addIdLibro($this);
        }

        return $this;
    }

    public function removeIdAutor(Autores $idAutor): self
    {
        if ($this->idAutor->contains($idAutor)) {
            $this->idAutor->removeElement($idAutor);
            $idAutor->removeIdLibro($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tematicas[]
     */
    public function getIdTematica(): Collection
    {
        return $this->idTematica;
    }

    public function addIdTematica(Tematicas $idTematica): self
    {
        if (!$this->idTematica->contains($idTematica)) {
            $this->idTematica[] = $idTematica;
        }

        return $this;
    }

    public function removeIdTematica(Tematicas $idTematica): self
    {
        if ($this->idTematica->contains($idTematica)) {
            $this->idTematica->removeElement($idTematica);
        }

        return $this;
    }

}
