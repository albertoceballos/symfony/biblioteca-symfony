<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Editoriales
 *
 * @ORM\Table(name="editoriales")
 * @ORM\Entity
 */
class Editoriales
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_editorial", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEditorial;

    /**
     * @var string
     *
     * @ORM\Column(name="editorial", type="string", length=255, nullable=false)
     */
    private $editorial;
    
     /**
     * @ORM\OneToMany(targetEntity="Libros", mappedBy="idEditorial")
     */
     private $libros;

     public function __construct()
     {
         $this->libros = new ArrayCollection();
     }


     public function getIdEditorial()
    {
        return $this->idEditorial;
    }

    public function getEditorial()
    {
        return $this->editorial;
    }

    public function setEditorial(string $editorial): self
    {
        $this->editorial = $editorial;

        return $this;
    }

    /**
     * @return Collection|Libros[]
     */
    public function getLibros(): Collection
    {
        return $this->libros;
    }

    public function addLibro(Libros $libro): self
    {
        if (!$this->libros->contains($libro)) {
            $this->libros[] = $libro;
            $libro->setIdEditorial($this);
        }

        return $this;
    }

    public function removeLibro(Libros $libro): self
    {
        if ($this->libros->contains($libro)) {
            $this->libros->removeElement($libro);
            // set the owning side to null (unless already changed)
            if ($libro->getIdEditorial() === $this) {
                $libro->setIdEditorial(null);
            }
        }

        return $this;
    }


}
