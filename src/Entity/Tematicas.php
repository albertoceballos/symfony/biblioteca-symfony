<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tematicas
 *
 * @ORM\Table(name="tematicas", indexes={@ORM\Index(name="id_padre", columns={"id_padre"})})
 * @ORM\Entity
 */
class Tematicas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_tematica", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTematica;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tematica", type="string", length=255, nullable=true)
     */
    private $tematica;

    /**
     * @var \Tematicas
     *
     * @ORM\ManyToOne(targetEntity="Tematicas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id_tematica")
     * })
     */
    private $idPadre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Libros", mappedBy="idTematica")
     */
    private $idLibro;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idLibro = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdTematica()
    {
        return $this->idTematica;
    }

    public function getTematica()
    {
        return $this->tematica;
    }

    public function setTematica(string $tematica): self
    {
        $this->tematica = $tematica;

        return $this;
    }

    public function getIdPadre()
    {
        return $this->idPadre;
    }

    public function setIdPadre(self $idPadre): self
    {
        $this->idPadre = $idPadre;

        return $this;
    }

    /**
     * @return Collection|Libros[]
     */
    public function getIdLibro(): Collection
    {
        return $this->idLibro;
    }

    public function addIdLibro(Libros $idLibro): self
    {
        if (!$this->idLibro->contains($idLibro)) {
            $this->idLibro[] = $idLibro;
            $idLibro->addIdTematica($this);
        }

        return $this;
    }

    public function removeIdLibro(Libros $idLibro): self
    {
        if ($this->idLibro->contains($idLibro)) {
            $this->idLibro->removeElement($idLibro);
            $idLibro->removeIdTematica($this);
        }

        return $this;
    }

}
