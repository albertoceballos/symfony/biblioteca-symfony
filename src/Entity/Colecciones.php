<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Colecciones
 *
 * @ORM\Table(name="colecciones")
 * @ORM\Entity
 */
class Colecciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_coleccion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idColeccion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coleccion", type="string", length=255, nullable=true)
     */
    private $coleccion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="capitulos", type="integer", nullable=true)
     */
    private $capitulos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="leidos", type="integer", nullable=true)
     */
    private $leidos;

    public function getIdColeccion()
    {
        return $this->idColeccion;
    }

    public function getColeccion()
    {
        return $this->coleccion;
    }

    public function setColeccion(string $coleccion): self
    {
        $this->coleccion = $coleccion;

        return $this;
    }

    public function getCapitulos()
    {
        return $this->capitulos;
    }

    public function setCapitulos(int $capitulos): self
    {
        $this->capitulos = $capitulos;

        return $this;
    }

    public function getLeidos()
    {
        return $this->leidos;
    }

    public function setLeidos(int $leidos): self
    {
        $this->leidos = $leidos;

        return $this;
    }


}
