<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Autores
 *
 * @ORM\Table(name="autores")
 * @ORM\Entity
 */
class Autores
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_autor", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAutor;

    /**
     * @var string
     *
     * @ORM\Column(name="autor", type="string", length=255, nullable=false)
     */
    private $autor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nacionalidad", type="string", length=255, nullable=true)
     */
    private $nacionalidad;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Libros", inversedBy="idAutor")
     * @ORM\JoinTable(name="escriben",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_autor", referencedColumnName="id_autor")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_libro", referencedColumnName="id_libro")
     *   }
     * )
     */
    private $idLibro;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idLibro = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdAutor()
    {
        return $this->idAutor;
    }

    public function getAutor()
    {
        return $this->autor;
    }

    public function setAutor(string $autor): self
    {
        $this->autor = $autor;

        return $this;
    }

    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    public function setNacionalidad(string $nacionalidad): self
    {
        $this->nacionalidad = $nacionalidad;

        return $this;
    }

    /**
     * @return Collection|Libros[]
     */
    public function getIdLibro(): Collection
    {
        return $this->idLibro;
    }

    public function addIdLibro(Libros $idLibro): self
    {
        if (!$this->idLibro->contains($idLibro)) {
            $this->idLibro[] = $idLibro;
        }

        return $this;
    }

    public function removeIdLibro(Libros $idLibro): self
    {
        if ($this->idLibro->contains($idLibro)) {
            $this->idLibro->removeElement($idLibro);
        }

        return $this;
    }

}
