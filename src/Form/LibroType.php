<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use App\Entity\Editoriales;

class LibroType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('titulo', TextType::class, [
            'label' => 'Título',
        ])
        ->add('anio', TextType::class, [
            'label' => 'Año',
        ])
        ->add('isbn', TextType::class, [
            'label' => 'ISBN',
        ])
        ->add('titulo', ChoiceType::class, [
            'label' => 'Título',
            'choices' => [
            'No' => 0,
            'Sí' => 1,
            ],
        ])
        ->add('idEditorial', EntityType::class, [
        // looks for choices from this entity
            'class' => Editoriales::class,
        // uses the User.username property as the visible option string
            'choice_label' => 'editorial',
            'label' => "Editorial:",
         ])
        ->add('descripcion', TextareaType::class,[
           'label'=>'Descripción',
           'label_attr'=>['class'=>'label_descripcion'],
           'attr'=>[
               'class'=>'descripcion',
            ]
        ])
        ->add('submit', SubmitType::class,[
            'label'=>'Guardar',
        ]);
    }

}
