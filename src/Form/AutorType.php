<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Doctrine\ORM\EntityRepository;

use App\Entity\Autores;

class AutorType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('autores', EntityType::class,[
            'class'=> Autores::class,
            'choice_label'=>'autor',
            'query_builder'=>function(EntityRepository $er){
                return $er->createQueryBuilder('a')->orderBy('a.autor','ASC');
            },
            
        ]);
                
    }
}



