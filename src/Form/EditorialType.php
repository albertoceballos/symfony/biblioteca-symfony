<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Editoriales;

class EditorialType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->add('editoriales', EntityType::class,[
           'class'=> Editoriales::class,
            'choice_label'=>'editorial',
        ]);
    }
    
}

