<?php

namespace App\Repository;

use App\Entity\Libros;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Libros|null find($id, $lockMode = null, $lockVersion = null)
 * @method Libros|null findOneBy(array $criteria, array $orderBy = null)
 * @method Libros[]    findAll()
 * @method Libros[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LibrosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Libros::class);
    }

    // /**
    //  * @return Libros[] Returns an array of Libros objects
    //  */
    
    public function busquedaPorTitulo($busqueda)
    {
        
        $em= $this->getEntityManager();
        $dql=$em->createQuery("SELECT l FROM App\Entity\Libros l WHERE l.titulo LIKE :busqueda")
                ->setParameter('busqueda','%'.$busqueda.'%');
        $libros=$dql->getResult();
        
        return $libros;
        
        /*return $this->createQueryBuilder('l')
            ->('l.titulo = :busqueda')
            ->setParameter('busqueda','%'.$busqueda.'%')
            ->getQuery()
            ->getResult();*/
    }
    

    /*
    public function findOneBySomeField($value): ?Libros
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
