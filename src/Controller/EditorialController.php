<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Editoriales;
use App\Form\EditorialType;

class EditorialController extends AbstractController {

    public function index(Request $request) {
        $editoriales_repo = $this->getDoctrine()->getRepository(Editoriales::class);
        $editoriales = $editoriales_repo->findAll();

        $form = $this->createForm(EditorialType::class);
        $form->handleRequest($request);
        
        //Si pulsamos el select y se envía el formulario
        if ($form->isSubmitted()) {
            //extraigo el idEditorial del select del formulario
            $datos = $request->request->get('editorial');
            $idEditorial = $datos['editoriales'];

            //hago un find para conseguir el objeto de la editorial, y a continuación
            // el getLibros() para conseguir la colección de libros que contiene:
            $editorial = $editoriales_repo->find($idEditorial);
            $libros_editorial = $editorial->getLibros();

            foreach ($libros_editorial as $libro) {
                //codifico las fotos que tengo en formato BLOB y las devuelvo en un array con índice con cada IDLibro
                $fotos[$libro->getIdLibro()] = base64_encode(stream_get_contents($libro->getFoto()));
            }

            return $this->render('editorial/index.html.twig', [
                        'editoriales' => $editoriales,
                        'form' => $form->createView(),
                        'libros' => $libros_editorial,
                        'fotos'=>$fotos,
            ]);
        }


        return $this->render('editorial/index.html.twig', [
                    'editoriales' => $editoriales,
                    'form' => $form->createView(),
        ]);
    }

}
