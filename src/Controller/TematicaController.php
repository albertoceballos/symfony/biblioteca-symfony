<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Tematicas;
use App\Entity\Libros;

class TematicaController extends AbstractController
{
    
    public function index()
    {
        //Para sacar las temáticas:
        $tematicas_repo= $this->getDoctrine()->getRepository(Tematicas::class);
        $tematicas=$tematicas_repo->findBy(['idPadre'=>1],['tematica'=>'ASC']);
       
        //para sacar las subtemáticas sin las temáticas generales:
        $em= $this->getDoctrine()->getManager();
        $dql=$em->createQuery('SELECT t FROM App\Entity\Tematicas t WHERE t.idPadre != 1 ORDER BY t.tematica');
        $subtematicas=$dql->getResult();
        return $this->render('tematica/index.html.twig', [
           'tematicas'=>$tematicas,
           'subtematicas'=>$subtematicas,
        ]);
    }
    
    public function muestra($id){
        
         //Para sacar las temáticas:
        $tematicas_repo= $this->getDoctrine()->getRepository(Tematicas::class);
        $tematicas=$tematicas_repo->findBy(['idPadre'=>1],['tematica'=>'ASC']);
       
        $tematica_sel=$tematicas_repo->findBy(['idTematica'=>$id],['tematica'=>'ASC']);   
        $tematica=$tematicas_repo->findOneBy(['idTematica'=>$id]);
        $nombre_tematica=$tematica->getTematica();
        
        $idPadre=$tematica->getIdPadre()->getIdTematica();   
        $subtematicas_tematica_sel=null;
        if($idPadre==1){
            $subtematicas_tematica_sel=$tematicas_repo->findBy(['idPadre'=>$id]);
            dump($subtematicas_tematica_sel);
        }
        
        
        foreach($tematica_sel as $tematica){
            $libros=$tematica->getIdLibro();
        }
        
        foreach($libros as $libro){
            $fotos[$libro->getIdLibro()]= base64_encode(stream_get_contents($libro->getFoto()));
        }
        
        
        //para sacar las subtemáticas sin las temáticas generales:
        $em= $this->getDoctrine()->getManager();
        $dql=$em->createQuery('SELECT t FROM App\Entity\Tematicas t WHERE t.idPadre != 1 ORDER BY t.tematica');
        $subtematicas=$dql->getResult();
        return $this->render('tematica/index.html.twig', [
           'tematicas'=>$tematicas,
           'subtematicas'=>$subtematicas,
           'libros'=>$libros,
           'fotos'=>$fotos,
           'nombre_tematica' =>$nombre_tematica,
           'subtematicas_sel'=>$subtematicas_tematica_sel,
        ]);
        
        
    }
}
