<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Autores;
use App\Entity\Libros;
use App\Form\AutorType;

class AutorController extends AbstractController {

    public function index(Request $request) {

        $autores_repo = $this->getDoctrine()->getRepository(Autores::class);
      
        /* hago un findBy con el primer parámetro vacío, saco todos los resultados como
          con un findAll pero los puedo ordenar como quiera, en seste caso alfabeticamente en orden ascendente */
        $autores = $autores_repo->findBy([], ['autor' => 'ASC']);
        
        $form_autores = $this->createForm(AutorType::class);
        $form_autores->handleRequest($request);


        if ($form_autores->isSubmitted()) {
            //recojo el id del autor del formulario
            $datos = $request->request->get('autor');
            $idAutor = $datos['autores'];

            //hago un find del autor y extraigo la coleccion de libros que trae
            $autor = $autores_repo->find($idAutor);
            $libros_autor = $autor->getIdLibro();

            foreach ($libros_autor as $libro) {
                //codifico las fotos que tengo en formato BLOB y las devuelvo en un array con índice con cada IDLibro
                $fotos[$libro->getIdLibro()] = base64_encode(stream_get_contents($libro->getFoto()));
            }

            return $this->render('autor/index.html.twig', [
                        'autores' => $autores,
                        'form_autores' => $form_autores->createView(),
                        'libros' => $libros_autor,
                        'fotos' => $fotos,
            ]);
        }




        return $this->render('autor/index.html.twig', [
                    'autores' => $autores,
                    'form_autores' => $form_autores->createView(),
        ]);
    }

}
