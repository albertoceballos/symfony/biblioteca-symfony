<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;

//Entidades:
use App\Entity\Libros;

//formulario para rellenar libro
use App\Form\LibroType;

class LibrosController extends AbstractController
{
   
    public function index()
    {
        $libros_repo= $this->getDoctrine()->getRepository(Libros::class);
        $libros=$libros_repo->findAll();
        
        
         foreach($libros as $libro){               
            //codifico las fotos que tengo en formato BLOB y las devuelvo en un array con índice con cada IDLibro
            $fotos[$libro->getIdLibro()]= base64_encode(stream_get_contents($libro->getFoto()));          
         }       
        return $this->render('libros/index.html.twig', [
            'libros'=>$libros,
            'fotos'=>$fotos,
        ]);
    }
    public function detail($id){
        
        $libros_repo= $this->getDoctrine()->getRepository(Libros::class);
        $libro=$libros_repo->find($id);
        
        $foto=base64_encode(stream_get_contents($libro->getFoto()));   
        
        return $this->render('libros/detail.html.twig',[
            'libro'=>$libro,
            'foto'=>$foto,
            
        ]);
    }
    
    public function edit(Request $request, Libros $libro){
        
        $form= $this->createForm(LibroType::class,$libro);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
            $em=$this->getDoctrine()->getManager();
            
            $em->persist($libro);
            $em->flush();
            
            return $this->redirect($this->generateUrl('libro_detail',['id'=>$libro->getIdLibro()]));
        }
        
        return $this->render('libros/create.html.twig',[
           'form'=>$form->createView(), 
        ]);
    }
    
    public function busquedaAjax(Request $request){
        
        if($request->isXmlHttpRequest()){
            $busqueda=$request->request->get('buscar');
            $libros_repo= $this->getDoctrine()->getRepository(Libros::class);
            $libros=$libros_repo->busquedaPorTitulo($busqueda);
            
            foreach ($libros as $libro){
                 $fotos[$libro->getIdLibro()]= base64_encode(stream_get_contents($libro->getFoto()));
            }
            return $this->render('libros/busqueda.html.twig',[
                'libros'=>$libros,
                'fotos'=>$fotos,
            ]);
        }
    }
}
