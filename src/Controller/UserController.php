<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;

//entidad Users
use App\Entity\Users;

//Clase formulario de registro:
use App\Form\RegisterType;

//Para la utenticación de usuarios:
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
  
    public function register(Request $request,UserPasswordEncoderInterface $encoder)
    {
        $user=new Users();
        //creo el formulario previamente escrito en su clase y le paso el objeto de usuario:
        $form=$this->createForm(RegisterType::class,$user);
        
        $form->handleRequest($request);   
        
        
        //Compruebo que se envía el formulario y que el formato de validación es válido:
        if($form->isSubmitted() && $form->isValid()){
            //establezo el rol del usuario:
            $user->setRole('ROLE_USER');
           
            //Codifico los datos del password y hago el setPassword
            $encoded=$encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($encoded);
         
            
            $em= $this->getDoctrine()->getManager();
            //guardo los datos en el objeto con persist:
            $em->persist($user);
            //guardo el objeto en la BBDD:
            $em->flush();
            //acabado el guardado redirijo a la ruta de inicio:
            return $this->redirectToRoute('libros_index');
        }
        
        return $this->render('user/register.html.twig', [
            'form'=>$form->createView(),
        ]);
    }
    
    public function login(AuthenticationUtils $authUtils){
        
        $error=$authUtils->getLastAuthenticationError();
        $last_username=$authUtils->getLastUsername();
        
        return $this->render('user/login.html.twig',[
            'error'=>$error,
            'last_username'=>$last_username,
        ]);
        
    }
    
}
